//
//  Array.cpp
//  CommandLineTool
//
//  Created by Duncan Whale on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "Array.h"
#include <iostream>

Array::Array()
{
    arrayPointer = nullptr;
    arraySize = 0;
}

Array::~Array()
{
    delete[] arrayPointer;
}

void Array::add(float itemValue)
{
    // Create a new pointer to point to a new array (in the heap).
    float* newArrayPointer = nullptr;
    if (newArrayPointer == nullptr)
        newArrayPointer = new float[arraySize + 1];
    
    // Copy old array values into the new array.
    for (int i = 0; i < arraySize; i++)
        newArrayPointer[i] = arrayPointer[i];
    
    // Add in the new value.
    newArrayPointer[arraySize] = itemValue;
    
    // Delete the old array and re-assign the original pointer. (New pointer is now defunkt.)
    delete[] arrayPointer;
    arrayPointer = newArrayPointer;
    
    // Update the array size.
    arraySize += 1;
}

float Array::get(int index) const
{
    return arrayPointer[index];
}

int Array::size() const
{
    return arraySize;
}