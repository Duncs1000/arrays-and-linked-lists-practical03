//
//  Array.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef ARRAY
#define ARRAY

#include <iostream>

class Array
{
public:
    // Default constructor.
    Array();
    
    // Destructor.
    ~Array();
    
    // To add in a new value.
    void add(float itemValue);
    
    // To find out what a particular value is.
    float get(int index) const;
    
    // To find out the array size.
    int size() const;
    
private:
    int arraySize;
    float* arrayPointer;
};

#endif /* Defined "Array" */