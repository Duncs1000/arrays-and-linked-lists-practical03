//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef LINKED_LIST
#define LINKED_LIST

#include <iostream>

class LinkedList
{
public:
    LinkedList();
    
    ~LinkedList();
    
    void add(float itemValue);
    
    float get(int index);
    
    int size();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    
    Node* head;
};

#endif /* Defined LinkedList */
