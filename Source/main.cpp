//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

bool testArray();

int main (int argc, const char* argv[])
{
    if (testArray() == true)
        std::cout << "Success!\n";
    else
        std::cout << "Balls...\n";
    
    return 0;
}

bool testArray()
{
    Array testArrayObject;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f};
    const int testArraySize = sizeof(testArray);
    
    // Test initialisations.
    if (testArrayObject.size() != 0)
    {
        std::cout << "Initial size is incorrect.\n";
        return false;
    }
    
    // Test the add function.
    for (int i = 0; i < testArraySize; i++)
    {
        testArrayObject.add(testArray[i]);
        
        if (testArrayObject.size() != i + 1)
        {
            std::cout << "Update of size failed.\n";
            return false;
        }
        
        if (testArrayObject.get(i) != testArray[i])
        {
            std::cout << "Value at index " << i << " recalled incorrectly.\n";
            return false;
        }
    }
    
    return true;
}