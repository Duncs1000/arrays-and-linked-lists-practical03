//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Duncan Whale on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "LinkedList.h"

LinkedList::LinkedList()
{
    head = nullptr;
}

LinkedList::~LinkedList()
{
    
}

void LinkedList::add(float itemValue)
{
    Node* addressCount = head;
    Node* newNode = new Node;
    
    newNode->value = itemValue;
    newNode->next = nullptr;
    
    addressCount->next = head->next;
    
    while (addressCount->next != nullptr)
    {
        addressCount = addressCount->next;
    }
    addressCount->next = nullptr;
}

float LinkedList::get(int index)
{
}

int LinkedList::size()
{
}